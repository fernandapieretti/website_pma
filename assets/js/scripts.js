$(document).ready(function() {

    //Menus
    $('#nav button').click(function() {
        if ($('#nav').hasClass("active")) {
            $('#nav').removeClass("active");
            $('#mask').fadeOut();
        } else {
            $('#nav').addClass("active");
            $('#mask').fadeIn();
        }
    });

    $('#header-interno button').click(function() {
        if ($('#nav-interno').hasClass("active")) {
            $('#nav-interno').removeClass("active");
            $('#header-interno').removeClass("active");
            $('#mask').fadeOut();
        } else {
            $('#nav-interno').addClass("active");
            $('#header-interno').addClass("active");
            $('#mask').fadeIn();
        }
    });

    $('.btn-servicos .btn').click(function() {
        if ($(this).parent(".btn-servicos").hasClass("active")) {
            $(this).parent(".btn-servicos").removeClass("active");
        } else {
            $(this).parent(".btn-servicos").addClass("active");
        }
    });

    $("#nav .menu .item-submenu a").mouseenter(function() {
        $(this).addClass("active")
        $('#nav-suspenso').addClass("active");
        $(".nav-suspenso").removeClass("active");
        $(".nav-suspenso#" + $(this).attr("data-menu")).addClass("active");
    });

    $("#nav .menu .item-submenu a").mouseleave(function() {
        $(this).removeClass("active")
        $('#nav-suspenso').removeClass("active");
    });

    $("#nav-suspenso").mouseenter(function() {
        $(this).addClass("active")
    });

    $("#nav-suspenso").mouseleave(function() {
        $(this).removeClass("active")
    });

    $("#btn-close-nav-suspenso").click(function() {
        $("#nav .menu .item-submenu a").removeClass("active")
        $('#nav-suspenso').removeClass("active");
    });

    //Accordion
    $(".accordion").accordion({
        heightStyle: "content",
        activate: function(event, ui) {
            if (!$.isEmptyObject(ui.newHeader.offset())) {
                $('html:not(:animated), body:not(:animated)').animate({ scrollTop: ui.newHeader.offset().top }, 'slow');
            }
        }
    });



    // thumbnails.carousel.js jQuery plugin

    var conf = {
        center: true,
        backgroundControl: false
    };

    var cache = {
        $carouselContainer: $('.thumbnails-carousel').parent(),
        $thumbnailsLi: $('.thumbnails-carousel li'),
        $controls: $('.thumbnails-carousel').parent().find('.carousel-control')
    };

    function init() {
        cache.$carouselContainer.find('ol.carousel-indicators').addClass('indicators-fix');
        cache.$thumbnailsLi.first().addClass('active-thumbnail');

        if (!conf.backgroundControl) {
            cache.$carouselContainer.find('.carousel-control').addClass('controls-background-reset');
        } else {
            cache.$controls.height(cache.$carouselContainer.find('.carousel-inner').height());
        }

        if (conf.center) {
            cache.$thumbnailsLi.wrapAll("<div class='center clearfix'></div>");
        }
    }

    function refreshOpacities(domEl) {
        cache.$thumbnailsLi.removeClass('active-thumbnail');
        cache.$thumbnailsLi.eq($(domEl).index()).addClass('active-thumbnail');
    }

    function bindUiActions() {
        cache.$carouselContainer.on('slide.bs.carousel', function(e) {
            refreshOpacities(e.relatedTarget);
        });

        cache.$thumbnailsLi.click(function() {
            cache.$carouselContainer.carousel($(this).index());
        });
    }

    $.fn.thumbnailsCarousel = function(options) {
        conf = $.extend(conf, options);

        init();
        bindUiActions();

        return this;
    }


    $('.thumbnails-carousel').thumbnailsCarousel();

});